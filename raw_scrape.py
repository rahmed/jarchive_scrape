import requests
import html5lib
import codecs

JARCHIVE_SEASON = 'http://www.j-archive.com/showseason.php?season='
JARCHIVE_SHOW = 'http://www.j-archive.com/showgame.php?game_id='
ERROR_CHECK = r'<p class="error">'

def scrape_seasons():
    '''Pull down all of the seasons'''
    for season in tuple(range(1,29)) + ('trebekpilots', 'superjeopardy'):
        season = str(season)
        print season    
        r = requests.get(JARCHIVE_SEASON + season)
        with codecs.open('seasons/' + season, 'w', 'utf-8') as output:
            output.write(r.text)


def scrape_episodes():
    '''Pull down all episodes (very hacky)'''
    for show_id in range(1,4000):
        show_id = str(show_id)
        r = requests.get(JARCHIVE_SHOW + show_id)
        print show_id

        # bc the site is dumb and doesn't send error codes...
        if ERROR_CHECK in r.text:
            with open('shows/does_not_exist', 'a') as output:
                output.write(show_id + '\n')
            with codecs.open('shows/ERROR_' + show_id, 'w', 'utf-8') as output:
                output.write(r.text)                
        else:
            with codecs.open('shows/' + show_id, 'w', 'utf-8') as output:
                output.write(r.text)



if __name__ == '__main__':
    scrape_seasons()
    scrape_episodes()