from bs4 import BeautifulSoup
import codecs
import re

parse_title_re = re.compile(r'^(.*)(Show #)(\d+)(, aired )(.+)$')
parse_super_title_re = re.compile(r'^(.*)(show #)(\d+)(, aired )(.+)$')
parse_trebek_title_re = re.compile(r'^(Trebek pilot #)(\d+)( - )(.+)$')

for show_id in range (1,3859):
    data = dict()

    show_id = str(show_id)
    print show_id

    try:

        try: 
            f = open('shows/' + show_id, 'r')
        except IOError:
            print 'Not there'

        soup = BeautifulSoup(f)

        # Parse the show number and date out of the title, using regex
        if "Super Jeopardy!" in soup.title.string:
            match = re.match(parse_super_title_re, soup.title.string)
            data['show_number'] = "super" + match.group(3)
            data['date'] = match.group(5)
        
        elif "Trebek pilot" in soup.title.string:
            data['show_number'] = 'DO_ME_BY_HAND_LATER'
            data['date'] = 'DO_ME_BY_HAND_LATER'

        else:
            match = re.match(parse_title_re, soup.title.string)
            data['show_number'] = match.group(3)
            data['date'] = match.group(5)

        # contestant info
        i = 0
        for tag in soup.find_all('p', 'contestants'):
            data['contestant' + str(i)] = tag.a.string
            data['contestant' + str(i) + 'desc'] = \
                                            str(tag).partition('</a>, ')[2][:-4]
            i += 1

        # double jeopardy stuff
        h3s = soup.find_all('h3')
        if h3s:
            double_jeopardy_h3 = h3s[2]
            double_jeopardy_table = double_jeopardy_h3.next_sibling.next_sibling
            names = double_jeopardy_table.tr
            scores = names.next_sibling.next_sibling
            remarks = scores.next_sibling.next_sibling

            if not remarks:
                remarks = [""]

            # Bad juju if one contestant's nick is a substring of another's name
            # n.b. contents is sometimes a link, which hasn't been parsed out here
            # but could easily be parsed out with a conditional    
    
            for name, score, remark in zip(names, scores, remarks):
                if name.string in data['contestant0']:
                    data['double0score'] = score.string
                    if remark.contents:
                        data['double0remark'] = remark.contents[0]
                    else:
                        data['double0remark'] = ""
            
                elif name.string in data['contestant1']:
                    data['double1score'] = score.string
                    if remark.contents:
                        data['double1remark'] = remark.contents[0]
                    else:
                        data['double1remark'] = ""

                elif name.string in data['contestant2']:
                    data['double2score'] = score.string
                    if remark.contents:
                        data['double2remark'] = remark.contents[0]
                    else:
                        data['double2remark'] = ""

        # final jeopardy stuff
        final_table = soup.find('table', 'final_round')
        if final_table:
            data['final_category'] = final_table.find('td', 'category_name').string
            data['final_prompt'] = final_table.find('td', id='clue_FJ').string
    
            answers =\
                BeautifulSoup(final_table.tr.td.div['onmouseover']).find('table')
    
            def final_stuff(name, guess, outcome, wager):
                if name in data['contestant0']:
                    data['final0guess'] = guess
                    data['final0outcome'] = outcome
                    data['final0wager'] = wager
                elif name in data['contestant1']:
                    data['final1guess'] = guess
                    data['final1outcome'] = outcome
                    data['final1wager'] = wager
                elif name in data['contestant2']:
                    data['final2guess'] = guess
                    data['final2outcome'] = outcome
                    data['final2wager'] = wager


            def parse_final_jeopardy_table(row):
                name = row.td.string
                guess = row.td.next_sibling.string
                outcome = row.td['class'][0]
                wager = row.next_sibling.string
                final_stuff(name, guess, outcome, wager)

            try:
                row1 = answers.tr
            except:
                pass
            try:
                row2 = row1.next_sibling.next_sibling
            except:
                pass
            try:
                row3 = row2.next_sibling.next_sibling
            except:
                pass

            try:
                parse_final_jeopardy_table(row1)
            except:
                pass
            try:
                parse_final_jeopardy_table(row2)
            except:
                pass
            try:
                parse_final_jeopardy_table(row3)
            except:
                pass

            final_score_table = soup.find_all('h3')[3].next_sibling.next_sibling

            names = final_score_table.tr
            scores = names.next_sibling.next_sibling
            remarks = scores.next_sibling.next_sibling

            if not remarks:
                remarks = [""]

            for name, score, remark in zip(names, scores, remarks):
                if name.string in data['contestant0']:
                    data['final0score'] = score.string
                    data['final0remark'] = remark.string
                elif name.string in data['contestant1']:
                    data['final1score'] = score.string
                    data['final1remark'] = remark.string
                elif name.string in data['contestant2']:
                    data['final2score'] = score.string
                    data['final2remark'] = remark.string


        # using semicolon as separator because there are commas in the data
        # switch to anything else if easier
        seperator = ';'
        formatted_output = show_id + seperator

        def format_output(category, formatted_output):
            try: 
                return formatted_output + data[category] + seperator
            except: 
                return formatted_output + seperator

        categories = [  'date',
                        'show_number',
                        'contestant0',
                        'contestant0desc',
                        'contestant1',
                        'contestant1desc',
                        'contestant2',
                        'contestant2desc',
                        'double0remark',
                        'double0score',
                        'double1score',
                        'double1remark',
                        'double2score',
                        'double2remark',
                        'final_category',
                        'final_prompt',
                        'final0outcome',
                        'final0wager',
                        'final0remark',
                        'final1guess',
                        'final1outcome',
                        'final1wager',
                        'final1remark',
                        'final2guess',
                        'final2outcome',
                        'final2wager',
                        'final2remark',
                        'final0score',
                        'final1score',
                        'final2score']

        for category in categories:
            formatted_output = format_output(category, formatted_output)

        formatted_output += "\n"

        with open('data', 'a') as output:
            output.write(formatted_output.encode('utf-8'))

    except:
        print "Funny business."
        with open('excepts', 'a') as output:
            output.write(show_id + "\n")
